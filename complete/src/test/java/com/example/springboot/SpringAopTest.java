package com.example.springboot;


import com.wzl.entity.UserService;
import com.wzl.entity.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.aop.aspectj.AspectJExpressionPointcutAdvisor;
import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @ClassName SpringAopTest
 * @Description SpringAopTest
 * @Author wuzhilang
 * @Date 2020/3/30 23:29
 * @Version 1.0
 */
@SpringBootTest
public class SpringAopTest {
//    代理对象的创建
    @Test
    public void createProxyTest(){
        Object target = new UserServiceImpl();
        ProxyFactory pf = new ProxyFactory(target);
        UserService service = (UserService)pf.getProxy();
        service.getName();
    }
// AspectJProxyFactory
    @Test
    public void createProxyAspectByTest(){
        Object target = new UserServiceImpl();
        AspectJProxyFactory pf = new AspectJProxyFactory(target);
        AspectJExpressionPointcutAdvisor advisor = new AspectJExpressionPointcutAdvisor();
        advisor.setExpression("execution(* *.getName(..))");
        pf.addAdvisor(advisor);
        UserService service = pf.getProxy();
        service.getName();
        service.setName();
    }
}
