package com.example.springboot;

import com.wzl.entity.UserService;
import com.wzl.entity.UserServiceImpl;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @ClassName CglibTest
 * @Description CglibTest
 * @Author wuzhilang
 * @Date 2020/3/30 21:27
 * @Version 1.0
 */
public class CglibTest {
    public static void main(String[] args) {
        UserServiceImpl  userServiceImpl = new UserServiceImpl();
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(UserServiceImpl.class);
        enhancer.setCallback(new MethodInterceptor() {
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                System.out.println("前置方法");
                Object result = null;
                try {
                    result = methodProxy.invoke(userServiceImpl, objects);
                } catch (Throwable throwable) {
                    System.out.println("异常通知");
                    throwable.printStackTrace();
                }
                System.out.println("后置方法");
                return result;
            }
        });
        UserService userService = (UserService) enhancer.create();
        userService.getName();
    }
}
