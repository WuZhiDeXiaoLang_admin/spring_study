package com.example.springboot;

import com.wzl.entity.UserService;
import com.wzl.entity.UserServiceImpl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * wzl 动态代理的测试类
 */
public class JavaProxyTest {
    public static void main(String[] args) {
        final UserServiceImpl userServiceImpl = new UserServiceImpl();
        UserService userService = (UserService) Proxy.newProxyInstance(JavaProxyTest.class.getClassLoader(),
                new Class[]{UserService.class}, new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        System.out.println("前置通知");
                        try {
                            method.invoke(userServiceImpl, args);
                        } catch (IllegalAccessException e) {
                            System.out.println("异常通知");
                            e.printStackTrace();
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                        System.out.println("后置通知");
                        return null;
                    }
                });
        userService.getName();
    }
}
