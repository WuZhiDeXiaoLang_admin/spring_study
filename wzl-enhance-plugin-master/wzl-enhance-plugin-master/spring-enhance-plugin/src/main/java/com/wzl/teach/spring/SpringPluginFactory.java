package com.wzl.teach.spring;

import java.util.List;

public interface SpringPluginFactory {
	
	/**
	 * 装载指定插件
	 * 
	 * @param pluginId
	 */
	public void activePlugin(String pluginId);

	/**
	 * 移除指定插件
	 * 
	 * @param pluginId
	 */
	public void disablePlugin(String pluginId);
	
	/**
	 * 安装插件
	 * @param plugin
	 */
	public void installPlugin(com.tuling.teach.spring.PluginConfig plugin, Boolean load);
	
	/**
	 * 卸载插件
	 * @param plugin
	 */
	public void uninstallPlugin(com.tuling.teach.spring.PluginConfig plugin);
	
	public List<com.tuling.teach.spring.PluginConfig> getPluginList();

}
